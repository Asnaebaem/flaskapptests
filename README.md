1. Klonujemy repo
2. Instalujemy pythona, jeżeli nie mamy
3. Tworzymy środowisko wirtualne dla naszego projektu:
	Odpalamy powershella i:
	pip install virtualenv //instalujemy globanie virtualenv
	
	Przechodzimy do katalogu z projektem i:
	virtualenv venv //Tworzymy środowisko wirtualne
	venv\Scripts\activate //Aktywujemy środowisko, jeśli nie będzie działać ze względu na execution policy to -> https://stackoverflow.com/questions/4037939/powershell-says-execution-of-scripts-is-disabled-on-this-system
	pip install -r requirements.txt //Ściągamy i instalujemy wszystkie paczki wymienione w tym pliku https://pip.pypa.io/en/stable/user_guide/#requirements-files
4. Odpalamy projekt w pyCharmie
5. Run -> Run, i powinno wszystko śmigać na 127.0.0.1:5000