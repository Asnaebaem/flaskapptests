from flask import Flask

app = Flask(__name__)


@app.route('/')
def Hello():
    return 'Hello World'


@app.route('/simpleops/<int:number>')
def Ops(number):
    val = (number / 2.0) * 3.1415
    return str(val)


if __name__ == '__main__':
    app.run()
